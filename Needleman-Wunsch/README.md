# Needleman - Wunsch 

<img src="./needle.svg" alt="Needleman - Wunsch backtracking figure">

## Note

LuaLaTeX implementation of the Needleman-Wunsch global sequence alignment algorithm.
